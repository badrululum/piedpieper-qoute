@extends('layouts.app')

@section('content')
<!-- {{'halo'}} -->

<div class="container"  >
    @if(session('msg'))
        <div class="alert alert-success">
            <p>{{ session('msg') }}</p>
        </div>
    @endif

   
   <div class="row" style="margin:5px;">
            <form class="navbar-form navbar-left" action="/quotes" method="get">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search" name="search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
           </form>

        <div class="col-md-5 col-md-offset-4">
            <a href='/quotes' class="btn btn-primary">
                All
            </a>
            <a href='/quotes/random' class="btn btn-primary">
                Random
            </a>
            <a href='/quotes/create' class="btn btn-primary">
                Create
            </a>
        </div>
    </div>

    <br> 
    <div class="row" style=" display:flex">
        @foreach($quote as $q)
        <div class="cards-columns" style="width:360px; border: 1px solid #eaeaea; border-radius:5px; padding: 1.25rem 1.5rem; margin: 10px;">
            <div class="blockquote">
                <div class="caption">
                    {{ $q->title }}
                </div>
                <p><a href="/quotes/{{$q->slug}}" class="btn btn-primary">show quotes</a></p>
            </div>
        </div>
       @endforeach
    </div>
</div>
@endsection