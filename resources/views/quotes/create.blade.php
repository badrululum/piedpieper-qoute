@extends('layouts.app')

@section('content')
<div class="container">

    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="/quotes" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title"  class="form-control col-sm-4" value="{{ old('title') }}" placeholder="what is the title . .">
    </div>
    
    <div class="form-group">
        <label for="content">Content</label>
        <textarea type="text" name="content" id="content"  class="form-control col-sm-4" placeholder="what is the content . .">{{ old('content') }}</textarea>
    </div>
    
    <button type="submit" class="btn btn-success">submit quotes</button>
    
    </form>
</div>
@endsection
