@extends('layouts.app')

@section('content')
<div class="container">

    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="/quotes/{{$quote->id}}" method="post">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title"  class="form-control col-sm-4" value="{{ old('title') ? old('title') : $quote->title   }}" placeholder="what is the title . .">
    </div>
    
    <div class="form-group">
        <label for="content">Content</label>
        <textarea type="text" name="content" id="content"  class="form-control col-sm-4" placeholder="what is the content . .">{{ old('content') ? old('content') : $quote->content }}</textarea>
    </div>
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">    
    <button type="submit" class="btn btn-success">submit quotes</button>
    
    </form>
</div>
@endsection
