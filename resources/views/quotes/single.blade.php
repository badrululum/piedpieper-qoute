@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron">
        <h2>{{ $quote->title }}</h2>
        <p>{{ $quote->content }}</p>
        <h4>Ditulis oleh : <a href="/profile/{{ $quote->user->id }}"> {{ $quote->user->name }}</a></h4>

        <p><a href="/quotes" class="btn btn-primary">Back to list</a></p>

        @if($quote->isOwner())
        <div>
        <a href="/quotes/{{$quote->id}}/edit" class="btn btn-warning">Edit</a>
        <form style="float:left;" method="POST" action="/quotes/{{$quote->id}}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger">delete</button>
            </form>
        </div>
        @endif
    
    </div>
</div>  
@endsection
