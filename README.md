# PiedPieper Qoute

Website accommodate quotes the dormant

Make with laravel
    - mysql

## START WITH BISMILLAH, ALLAH WITH ME :)

### sistem authentikasi user
kita mulai dengan membuat sistem authentikasi user, dari migrasi, model dan view yang automatis untuk login juga register pada laravel


### membuat table kutipan
Bagimana dengan struktur table database dari kutipan itu sendiri, kolom apa saja yang kita butuhkan


### model dan relasi kutipan
Membuat model dari kutipan dan mengatur relasinya dengan user pada model masing-masing


### form memasukkan data
Menyiapkan form agar user bisa mulai memasukkan kolom-kolom yang diperlukan untuk data kutipannya


### input database dan validasi error
Memasukkan data kutipan ke database beserta validasi error yang dibutuhkan


### list daftar kutipan dan pesan berhasil
Menampilkan daftar kutipan pada index, kita juga akan menyertakan feedback berupa alert kepada user, agar UX nya lebih baik


### Halaman single
Halaman single atau halaman detail dari masing-masing kutipan, setiap user bisa melihat detail dari kutipan yang diinginkan


### tombol edit dan delete
Membuat tombol edit dan delete untuk memudahkan user memanipulasi data kutipan yang dibuat oleh mereka masing-masing


### edit dan hapus kutipan
Membuat fungsi edit dan hapus itu sendiri pada controller agar perubahan data bisa tersambung dengan database


### generate kutipan random
Membuat tombol dan fungsi untuk mengacak daftar kutipan dan menampilkannya ke user


### halaman profile
Halaman profile untuk user yang sedang login, lengkap dengan data kutipan yang sudah dibuat


### melihat profil orang lain
Melihat halaman profile orang lain dengan mengeluarkan route yang sudah kita buat dari group middleware authentikasi


### menyiapkan komentar
Menyiapkan database dari sistem komentar yang akan digunakan pada setiap halaman kutipan


### membuat komentar
Mulai membuat komentar dan fungsi untuk memasukkanya ke database agar bisa ditampilkan langsung pada halaman singlenya


### edit dan hapus komentar
sama seperti kutipannya, user yang membuat komentar juga boleh menghapus atau mengedit komentar masing-masing


### debugbar dan eager loading
install laravel debugbar untuk melihat detail penggunaan laravel pada setiap halaman, kita juga akan mengimprove query yang digunakan dengan eager loading


### menyiapkan table kategori
siapkan table kategori untuk sistem tag atau pengkatogerian dari masing-masing kutipan agar lebih mudah dikelompokkan


### user input tag
Kita akan menggunaka dropdown sebagai media untuk user memasukkan daftar tag yang diinginkan


### edit tag sebelumnya
Bagaimana dengan cara mengedit tag yang sebelumnya sudah diinput oleh user, metode apa yang digunakan untuk table pivotnya


### filter search dan tag
Mengimplementasikan Fungsi Search berdasarkan judul kutipan dan juga filter berdasarkan tag atau kategori masing masing


### migrasi dan model likes
Menyiapkan migrasi dan model untuk sistem likes, yap seperti pada facebook, masing-masin postingan bisa di like oleh user, kita akan lihat bagaimana cara membuat sistem like disini


### input like ke database
Memasukkan data like yang dibuat lewat ajax ke database


### feedback dan validasi
memberikan feedback pada frontend yaitu untuk user agar jelas apa yang terjadi ketika user melakuka action tertentu


### sistem unlike
Bukan hanya like tapi user juga bisa unlike atau mencancel favorit kutipan sebelumnya


### refactor trait dan component
DRY prinsip utama dalam programming yaitu dont repeat yourself. Kita akan melakukan refactor untuk sistem like yang baru saja kita buat


### notifikasi bag1
Banyak yang bertanya bagaimana cara membuat sistem notifikasi pada sekolahkoding, disini akan kita lihat cara mengirim pemberitahuan ke user yang relevan dari action tertentu


### notifikasi bag2
Melanjutkan sistem notifikasi sebelumnya, kita lihat bagaimana membuat halaman notifikasi dan mengupdate statusnya sudah terlihat atau belum


### improve sistem tag
Masih ada selalu yang bisa kita improve, kali ini sistem tag yang sebelumnya tidak sempurna ketika user tidak lulus validasi membuat kutipan akan kita perbaiki


