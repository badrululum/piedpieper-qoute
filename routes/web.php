<?php


Route::group(['middleware' => 'auth'], function()
{
     // --- Quotes   
    
    Route::get('/quotes/create', 'QuoteController@create');
    Route::post('/quotes', 'QuoteController@store');
    Route::get('/quotes/{id}/edit', 'QuoteController@edit');
    Route::put('/quotes/{id}', 'QuoteController@update');
    Route::delete('/quotes/{id}', 'QuoteController@destroy');

});

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/profile/{id?}', 'HomeController@profile');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/quotes', 'QuoteController@index');
Route::get('/quotes/random', 'QuoteController@random');
Route::get('/quotes/{slug}', 'QuoteController@show');



// Route::resource('quotes', 'QuoteController');