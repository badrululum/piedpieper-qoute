<?php

namespace App\Models;

use Auth;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'title', 'slug', 'content', 'user_id'
    ];
    
    public function user()
    {
        // 1 kutipan hanya milik user tidak boleh banyak user
        return $this->belongsTo('App\Models\User');
    }
  
    public function isOwner()
    {
        if (Auth::guest()) {
            return false;
        }
        return Auth::user()->id == $this->user->id;
    }
}
