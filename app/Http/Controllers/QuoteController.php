<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Quote;
use App\Models\User;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quote = Quote::all();
        // var_dump($quotes);
        return view('quotes.index', compact('quote'));
        // return view('quotes.single', compact('quote'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
        // dd('hard');
        return view('quotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return die('hard');
        // die($request);
        $this->validate($request, [
            'title' => 'required|min:3',
            'title' => 'required|min:5',
        ]);
        
        $slug = str_slug($request->title, '-');
        //cek slug ngga kembar
        if (Quote::where('slug', $slug)->first() != null) {
            $slug = $slug . '-' .time();
        }

        $quotes = Quote::create([
            'title'    => $request->title,
            'slug'     => $slug,
            'content'  => $request->content,
            'user_id'  => Auth::user()->id,
            ]);
            // die(var_dump(Auth::user()));
            return redirect('quotes')->with('msg', 'kutipan berhasil disubmit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $quote = Quote::where('slug', $slug)->first();
        if (empty($quote)) {
            abort(404);
        }
        return view('quotes.single', compact('quote'));
    }

    public function random()
    {
        $quote = Quote::inRandomOrder()->first();
        if (empty($quote)) {
            abort(404);
        }
        return view('quotes.single', compact('quote'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quote = Quote::findOrfail($id);
        return view('quotes.edit', compact('quote'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quote = Quote::findOrfail($id);
        if ($quote->isOwner()) {
            $quote->update([
            'title' => $request->title,
            'content' => $request->content
             ]);
        } else {
            abort(403);
        }

        return redirect('quotes')->with('msg', 'kutipan berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quote = Quote::findOrFail($id);
        if ($quote->isOwner()) {
            $quote->delete();
        } else {
            abort(403);
        }
        return redirect('quotes')->with('msg', 'kutipan berhasil dihapus');
    }

    public function tes()
    {
        die('hard');
    }
}
